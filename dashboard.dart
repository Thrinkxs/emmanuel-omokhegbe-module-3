import 'package:emmanuel_omokhegbe_module_3/main.dart';
import 'package:emmanuel_omokhegbe_module_3/profile_edit.dart';
import 'package:flutter/material.dart';


class HomeScreem extends StatelessWidget {
  const HomeScreem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Dashboard")),
      body: Column(
        children: [
          FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return profileed();
                  },
                ),
              );
            },
            child: Text("Profile"),
          ),
          SizedBox(
            height: 5,
          ),
          FloatingActionButton(
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return LoginScreen();
                  },
                ),
                (route) => false,
              );
            },
            child: Text("Sign\nOut"),
          ),
        ],
      ),
    );
  }
} 